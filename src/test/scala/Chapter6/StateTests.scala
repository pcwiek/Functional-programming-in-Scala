package Chapter6

import org.scalatest._

class StateTests extends FlatSpec with Matchers {
  "flatMap" should "work as expected for State monad" in {
    val s = State[Int, String](s => (s.toString, s + 1))
    val fn = (str : String) => State[Int, Int](s => (str.length, s + 1))
    s.flatMap(fn).transition(10)._1 should equal (2)
    s.flatMap(fn).transition(10)._2 should equal (12)
  }

  "map" should "project the State value" in {
    val s = State[Int, String](s => (s.toString, s + 1))
    val mapping = s.map(s => s.contains("2"))
    mapping.transition(2)._1 should equal (true)
    mapping.transition(10)._1 should equal (false)
  }

  "map_1" should "work the same as map" in {
    val s = State[Int, String](s => (s.toString, s + 1))
    val mapping = s.map_1(s => s.contains("2"))
    mapping.transition(2)._1 should equal (true)
    mapping.transition(10)._1 should equal (false)
  }

  "map2" should "project both State values at the same time" in {
    val s1 = State[Int, String](s => (s.toString, s + 1))
    val s2 = State[Int, Double](s => (s.toDouble, s + 2))
    val transitioned = s1.map2(s2)((e1, e2) => e1.length + e2.toLong).transition(11)

    transitioned._1 should equal (14L)
    transitioned._2 should equal (14)
  }

  "sequence" should "transforma a list of state transitions into a state of list of elements" in {
    val lst = List[State[Int, String]](
      State[Int,String](s => (s.toString, s + 1)),
      State[Int,String](s => ((s % 2 == 0).toString.toUpperCase, s + 2))
    )

    val seq = State.sequence(lst)

    seq.transition(1)._1 should equal (List("1", "TRUE"))
  }
}
class FsmTests extends FlatSpec with Matchers {
  "Inserting a coin into a locked machine" should "unlock the machine if there is candy left" in {
    val machine = Machine(locked = true, 1, 0)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Coin)).transition(machine)
    candies should equal (1)
    coins should equal (1)
    resultMachine should equal (Machine(locked = false, 1, 1))
  }

  "Inserting a coin into a locked machine" should "not do anything if there is no candy left" in {
    val machine = Machine(locked = true, 0, 2)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Coin)).transition(machine)
    candies should equal (0)
    coins should equal (2)
    resultMachine should equal (Machine(locked = true, 0, 2))
  }

  "Turning the knob on an unlocked machine" should "cause it to dispense candy and become locked" in {
    val machine = Machine(locked = false, 1, 2)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Turn)).transition(machine)
    candies should equal (0)
    coins should equal (2)
    resultMachine should equal (Machine(locked = true, 0, 2))
  }

  "Turning the knob on a locked machine" should "do nothing" in {
    val machine = Machine(locked = true, 1, 2)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Turn)).transition(machine)
    candies should equal (1)
    coins should equal (2)
    resultMachine should equal (Machine(locked = true, 1, 2))
  }

  "Inserting a coin into unlocked machine" should "do nothing" in {
    val machine = Machine(locked = false, 1, 2)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Coin)).transition(machine)
    candies should equal (1)
    coins should equal (2)
    resultMachine should equal (Machine(locked = false, 1, 2))
  }

  "Machine that is out of candy" should "ignore all inputs" in {
    val machine = Machine(locked = true, 0, 2)
    val ((candies, coins), resultMachine) = FSM.simulate(List(Coin, Turn, Coin, Turn)).transition(machine)
    candies should equal (0)
    coins should equal (2)
    resultMachine should equal (Machine(locked = true, 0, 2))
  }
}
