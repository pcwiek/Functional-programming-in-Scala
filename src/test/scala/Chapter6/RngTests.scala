package Chapter6

import org.scalatest._

class RngTests extends FlatSpec with Matchers {
  "nonNegativeInteger" should "not return a negative int" in {
    val rng = SimpleRNG(-1)
    val (n, r) = rng.nextInt
    val (nonNeg, r2) = RNG.nonNegativeInt(rng)
    n should equal (-384749)
    nonNeg should equal (384748)
  }

  "double" should "return a double between 0 and 1" in {
    val rng = SimpleRNG(-1)
    val (d, r) = RNG.double(rng)
    def roundTo6(dbl : Double) = math.floor(d * 1000000) / 1000000
    roundTo6(d) should equal (roundTo6(384748.0 / Int.MaxValue))
  }

  "ints" should "generate n random integers" in {
    val rng = SimpleRNG(-1)
    val (list, _) = RNG.ints(5)(rng)
    list.length should equal (5)
  }

  "doubleViaMap" should "return a double between 0 and 1" in {
    val rng = SimpleRNG(-1)
    val (d, r) = RNG.doubleViaMap(rng)
    def roundTo6(dbl : Double) = math.floor(d * 1000000) / 1000000
    roundTo6(d) should equal (roundTo6(384748.0 / Int.MaxValue))
  }

  "map2" should "take two actions and return action that combines results" in {
    val rng = SimpleRNG(-1)
    val (t1, r1) = RNG.intDouble(rng)
    val (t2, r2) = RNG.map2(RNG.int, RNG.double)((_,_))(rng)
    t2 should equal (t1)
  }

  "sequence" should "transform a list of RAND to a RAND of list of elements" in {
    val rng = SimpleRNG(0)
    val list = List[RNG.Rand[Int]](r => r.nextInt, r => r.nextInt)
    RNG.sequence(list)(rng)._1 should equal (List(0, 4232237))
  }

  "mapViaFlatMap" should "work the same as map" in {
    val rng = SimpleRNG(0)
    val res1 = RNG.map(RNG.int)(i => i.toString)(rng)._1
    val res2 = RNG.mapViaFlatMap(RNG.int)(i => i.toString)(rng)._1
    res2 should equal (res1)
  }

  "map2ViaFlatMap" should "work the same as map2" in {
    val rng = SimpleRNG(-1)
    val (t1, r1) = RNG.intDouble(rng)
    val (t2, r2) = RNG.map2(RNG.int, RNG.double)((_,_))(rng)
    val (t3, r3) = RNG.map2ViaFlatMap(RNG.int, RNG.double)((_,_))(rng)
    t2 should equal (t1)
    t3 should equal (t2)
  }

}
