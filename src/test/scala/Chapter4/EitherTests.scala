package Chapter4

import org.scalatest._

class EitherTests extends FlatSpec with Matchers {
  "sequence" should "act like a monad transformer, turning a sequence of all Some values into a Right(List) or Left otherwise" in {
    val rights = (1 to 5).map(Right(_)).toList
    Either.sequence(rights) should equal (Right(List(1,2,3,4,5)))
    val oneLeft = rights :+ Left("Bad number!")
    Either.sequence(oneLeft) should equal (Left("Bad number!"))
  }

  "traverse" should "return Right if projection for all elements returns Right or first Left otherwise" in {
    val elements = (1 to 5).toList
    Either.traverse(elements)(e => if (e % 2 == 0) Right(e) else Left("Odd number!")) should equal (Left("Odd number!"))
    Either.traverse(elements)(e => if (e < 10) Right(e) else Left("A number bigger than 9")) should equal (Right(List(1,2,3,4,5)))
  }

}