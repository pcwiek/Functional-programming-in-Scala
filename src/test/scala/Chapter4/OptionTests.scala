package Chapter4

import org.scalatest._

class OptionTests extends FlatSpec with Matchers {
  "mean" should "return mean of a non-empty sequence" in {
    val source = Seq(1.0, 2.0, 3.0, 4.0, 5.0)
    Option.mean(source) should equal (Some(3))
  }

  "mean" should "return None if the sequence is empty" in {
    Option.mean(Seq()) should equal (None)
  }

  "map2" should "map both values if input options are Some" in {
    Option.map2(Some(4), Some(5))(_*_) should equal (Some(20))
  }

  "map2" should "return None if either (or both) of input options is None" in {
    Option.map2(Some(4), None : Option[Int])(_*_) should equal (None)
    Option.map2(None : Option[Int], Some(5))(_*_) should equal (None)
    Option.map2(None : Option[Int], None : Option[Int])(_*_) should equal (None)
  }

  "sequence" should "act like a monad transformer, turning a sequence of all Some values into a Some(List) or None otherwise" in {
    val somes = (1 to 5).map(Some(_)).toList
    Option.sequence(somes) should equal (Some(List(1,2,3,4,5)))
    val oneNone = somes :+ None
    Option.sequence(oneNone) should equal (None)
  }

  "traverse" should "return Some if projection for all elements returns Some or None otherwise" in {
    val elements = (1 to 5).toList
    Option.traverse(elements)(e => if (e % 2 == 0) Some(e) else None) should equal (None)
    Option.traverse(elements)(e => if (e < 10) Some(e) else None) should equal (Some(List(1,2,3,4,5)))
  }

  "traverseNaive" should "return Some if projection for all elements returns Some or None otherwise" in {
    val elements = (1 to 5).toList
    Option.traverseNaive(elements)(e => if (e % 2 == 0) Some(e) else None) should equal (None)
    Option.traverseNaive(elements)(e => if (e < 10) Some(e) else None) should equal (Some(List(1,2,3,4,5)))
  }

  "traverse2" should "return Some if projection for all elements returns Some or None otherwise" in {
    val elements = (1 to 5).toList
    Option.traverse2(elements)(e => if (e % 2 == 0) Some(e) else None) should equal (None)
    Option.traverse2(elements)(e => if (e < 10) Some(e) else None) should equal (Some(List(1,2,3,4,5)))
  }

}