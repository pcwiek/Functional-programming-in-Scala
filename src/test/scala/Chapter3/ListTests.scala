package Chapter3

import org.scalatest._

class ListTests extends FlatSpec with Matchers {
  "tailEx" should "throw exception on empty list" in {
    an [RuntimeException] should be thrownBy List.tailEx(Nil)
  }

  "tailEx" should "return tail of a non-empty list" in {
    List.tailEx(List(1,2,3)) should equal (List(2,3))
  }

  "tailEither" should "return Left on an empty list" in {
    List.tailEither(Nil) should equal (Left("Empty list has no tail"))
  }

  "tailEither" should "return Right tail of a non-empty list" in {
    List.tailEither(List(1,2,3)) should equal (Right(List(2,3)))
  }

  "tailOption" should "return None on an empty list" in {
    List.tailOption(Nil) should equal (None)
  }

  "tailOption" should "return Some tail of a non-empty list" in {
    List.tailOption(List(1,2,3)) should equal (Some(List(2,3)))
  }

  "setHead" should "set the head of an empty list" in {
    List.setHead(2, Nil) should equal (List(2))
  }

  "setHead" should "set the head of a non-empty list" in {
    List.setHead("Aaa", List("abc", "bcd")) should equal (List("Aaa", "bcd"))
  }

  "drop" should "return empty list if too many elements are dropped" in {
    List.drop(3, List(2,3)) should equal (Nil)
  }

  "drop" should "return remaining elements of the list" in {
    List.drop(2, List(2,3,4)) should equal (List(4))
  }

  "dropWhile" should "drop elements as long as predicate holds" in {
    List.dropWhile(List(2,4,7,6), (i : Int) => i % 2 == 0) should equal (List(7,6))
  }

  "init" should "get all elements but the lst from the list" in {
    List.init(List(1,2,3)) should equal (List(1,2))
  }

  "length" should "return correct list length" in {
    List.length(List(1,2,3)) should equal (3)

  }

  "sumViaFoldLeft" should "correctly sum a list of integers" in {
    List.sumViaFoldLeft(List(2,4,8)) should equal (14)
  }

  "productViaFoldLeft" should "correctly calcualte a product a list of integers" in {
    List.productViaFoldLeft(List(2,4,8)) should equal (64)
  }

  "lengthViaFoldLeft" should "return correct list length" in {
    List.lengthViaFoldLeft(List(1,2,8,81)) should equal (4)
  }

  "reverse" should "reverse the list" in {
    List.reverse(List(1,4,10,11)) should equal (List(11,10,4,1))
  }

  "append" should "append two lists together" in {
    List.append(List(1,2), List(5,3)) should equal (List(1,2,5,3))
  }

  "concat" should "concatenate a list of lists together" in {
    List.concat(List(List(1,4,2), List(8,1,2))) should equal (List(1,4,2,8,1,2))
  }

  "addOne" should "add 1 to each element of list of integers" in {
    List.addOne(List(3,11,9)) should equal (List(4,12,10))
  }

  "numbersToStrings" should "turn a list of doubles into a list of strings" in {
    List.numbersToStrings(List(2.0,3.3,11.21)) should equal (List("2.0", "3.3", "11.21"))
  }

  "map" should "correctly run a transformation on a list" in {
    List.map(List(3,4,12))(i => (i * 2).toDouble) should equal (List(6,8,24))
  }

  "filter" should "properly filter a list" in {
    List.filter(List(2,7,8,12,13))(i => i % 2 == 0) should equal (List(2,8,12))
  }

  "flatMap" should "correctly run a transformation on a list" in {
    List.flatMap(List(1,2))(i => List(i+1, i+2)) should equal (List(2,3,3,4))
  }

  "filterViaFlatMap" should "properly filter a list" in {
    List.filterViaFlatMap(List(2,7,8,12,13))(i => i % 2 == 0) should equal (List(2,8,12))
  }

  "addPairs" should "add elements pairwise" in {
    List.addPairs(List(1,2,3), List(4,5,6)) should equal (List(5,7,9))
  }

  "zipWith" should "run a pairwise transformation on both list with result in correct order" in {
    val l = List(1,2,3)
    val r = List(10,20,30)
    List.zipWith(l, r)(_*_) should equal (List(10, 40, 90))
  }

  "hasSubsequence" should "return true if there is a subsequence in a list" in {
    val source = List(8,21,33,41,22,141,2)
    val subseq1 = List(8)
    val subseq2 = List(22,141)
    val subseq3 = List(21,33,41,22)

    List.hasSubsequence(source, subseq1) should equal (true)
    List.hasSubsequence(source, subseq2) should equal (true)
    List.hasSubsequence(source, subseq3) should equal (true)
  }

  "hasSubsequence" should "return false if there is no subsequence in a list" in {
    val source = List(8,21,33,41,22,141,2)
    val noSubseq1 = List(9)
    val noSubseq2 = List(41,22,140)
    val noSubseq3 = List(2,1)

    List.hasSubsequence(source, noSubseq1) should equal (false)
    List.hasSubsequence(source, noSubseq2) should equal (false)
    List.hasSubsequence(source, noSubseq3) should equal (false)
  }
}

