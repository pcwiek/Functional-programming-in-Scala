package Chapter3

import org.scalatest._

class TreeTests extends FlatSpec with Matchers {
  "size" should "return correct node count (branches and leaves) for a tree" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.size(tree) should equal (9)
  }

  "maximum" should "return largest integer element" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.maximum(tree) should equal (11)
  }

  "depth" should "return the biggest available depth to a leaf" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.depth(tree) should equal (3)
  }

  "map" should "run transformation on tree elements" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    val expected = Branch(Branch(Leaf("2"), Leaf("4")), Branch(Leaf("2"), Branch(Leaf("8"), Leaf("22"))))
    Tree.map(tree)(e => (e * 2).toString) should equal (expected)
  }

  "fold" should "perform aggregation on tree elements" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.fold(tree)(identity)((b1, b2) => b1 * b2) should equal (88)
  }

  "sizeViaFold" should "return correct node count (branches and leaves) for a tree" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.sizeViaFold(tree) should equal (9)
  }

  "maximumViaFold" should "return largest integer element" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.maximumViaFold(tree) should equal (11)
  }

  "depthViaFold" should "return the biggest available depth to a leaf" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    Tree.depthViaFold(tree) should equal (3)
  }

  "mapViaFold" should "run transformation on tree elements" in {
    val tree = Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(1), Branch(Leaf(4), Leaf(11))))
    val expected = Branch(Branch(Leaf("2"), Leaf("4")), Branch(Leaf("2"), Branch(Leaf("8"), Leaf("22"))))
    Tree.mapViaFold(tree)(e => (e * 2).toString) should equal (expected)
  }

}
