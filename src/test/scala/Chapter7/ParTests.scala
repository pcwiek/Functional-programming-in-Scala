package Chapter7

import java.util.concurrent.{ExecutorService, Future, ForkJoinPool}

import Chapter7.Par.Par
import org.scalatest._

class ParTests extends FlatSpec with Matchers {
  private val es = new ForkJoinPool()
  "asyncF" should "fork and return async op" in {
    val result = Par.asyncF((i : Int) => i.toString)(10)
    result shouldBe a [(ExecutorService) => Future[String]]
    result(es).get() should equal ("10")
  }

  "sequence" should "transform a list of async operations into async operation for a list" in {
    val result = Par.sequence(List(Par.unit(4), Par.unit(10)))
    result shouldBe a [Par[List[Int]]]
    result(es).get() should equal (List(4,10))
  }

  "parFilter" should "lift filtering into async operation" in {
    val result = Par.parFilter(List(1,2,3))(_ % 2 != 0)
    result shouldBe a [Par[List[Int]]]
    result(es).get() should equal (List(1,3))
  }

  "choiceN" should "select n-th operation in a list based on async selector" in {
    val result = Par.choiceN(Par.unit(2))(List(3,29,11,22).map(Par.unit))
    result shouldBe a [Par[Int]]
    result(es).get() should equal (11)
  }

  "choiceMap" should "select matching operation from a map" in {
    val result = Par.choiceMap(Par.unit("third"))(Map("fst" -> Par.unit(1), "snd" -> Par.unit(22), "third" -> Par.unit(12)))
    result shouldBe a [Par[Int]]
    result(es).get() should equal (12)
  }

  "flatMap" should "work as expected (?)" in {
    val result = Par.flatMap(Par.unit(23))(i => Par.unit(i % 2 == 0))
    result shouldBe a [Par[Boolean]]
    result(es).get() should equal (false)
  }
}
