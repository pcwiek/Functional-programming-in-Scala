package Chapter5

import org.scalatest._

class StreamTests extends FlatSpec with Matchers {
  "toList" should "convert the stream to a list" in {
    val stream = Stream(4,3,2,4,2)
    stream.toList should equal (List(4,3,2,4,2))
  }

  "toListSimpleUnsafe" should "convert the stream to a list" in {
    val stream = Stream(4,3,2,4,2)
    stream.toListSimpleUnsafe should equal (List(4,3,2,4,2))
  }

  "toListImperative" should "convert the stream to a list" in {
    val stream = Stream(4,3,2,4,2)
    stream.toListImperative should equal (List(4,3,2,4,2))
  }

  "take" should "take given number of elements from the stream" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.take(2).toList should equal (Stream(1,2).toList)
  }

  "take" should "return empty string if the number taken is <= 0" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.take(0) should equal (Stream.empty)
    stream.take(-2) should equal (Stream.empty)
  }

  "drop" should "return a stream with first n elements dropped" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.drop(3).toList should equal (Stream(4,5,6).toList)
  }

  "drop" should "return an empty stream if dropped element count is larger than stream element count" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.drop(9) should equal (Stream.empty)
  }

  "takeWhile" should "take elements from the stream as long as predicate holds" in {
    val stream = Stream(2,4,6,77,2,11)
    stream.takeWhile(_ % 2 == 0).toList should equal (List(2,4,6))
  }

  "forAll" should "return true if all elements in the stream match the predicate" in {
    val stream = Stream(2,4,6,8)
    stream.forAll(_ % 2 == 0) should equal (true)
  }

  "forAll" should "return false if the stream is empty or at least one element does not match the predicate" in {
    val stream = Stream(2,4,5,8)
    stream.forAll(_ % 2 == 0) should equal (false)
  }

  "takeWhileViaFoldRight" should "take elements from the stream as long as predicate holds" in {
    val stream = Stream(2,4,6,77,2,11)
    stream.takeWhileViaFoldRight(_ % 2 == 0).toList should equal (List(2,4,6))
  }

  "headOption" should "return Some if the stream is non-empty" in {
    val stream = Stream(3,4,5)
    stream.headOption should equal (Some(3))
  }

  "headOption" should "return None if the stream is empty" in {
    Stream.empty.headOption should equal (None)
  }

  "mapViaFoldRight" should "return a stream after element-by-element projection" in {
    val stream = Stream(2,4)
    stream.mapViaFoldRight(x => (x * 2).toString).toList should equal (List("4", "8"))
  }

  "filterViaFoldRight" should "filter out stream elements that do not match the predicate" in {
    val stream = Stream(1,2,3,4,5)
    stream.filterViaFoldRight(_ % 2 == 0).toList should equal (List(2,4))
  }

  "appendViaFoldRight" should "append one stream to the end of another" in {
    val stream = Stream(1,2,3)
    val app = Stream(4,5,6)
    stream.appendViaFoldRight(app).toList should equal (List(1,2,3,4,5,6))
  }

  "constant" should "return infinite stream of constant values" in {
    val stream = Stream.constant(5)
    stream.take(5).toList should equal (List(5,5,5,5,5))
    stream.take(7).toList should equal (List(5,5,5,5,5,5,5))
  }

  "from" should "return monotonically increasing (until it overflows) stream of integers" in {
    val stream = Stream.from(10)
    stream.take(3).toList should equal (List(10, 11, 12))
  }

  "fibs" should "return the Fibonacci sequence lazily" in {
    Stream.fibs.take(7).toList should equal (List(0,1,1,2,3,5,8))
  }

  "unfold" should "corecursively generate a sequence" in {
    Stream.unfold(40)(i => if (i < 45) Some((i/2, i+1)) else None).toList should equal (List(20, 20, 21, 21, 22))
  }

  "fibsUnfolded" should "return the Fibonacci sequence lazily" in {
    Stream.fibsUnfolded.take(7).toList should equal (List(0,1,1,2,3,5,8))
  }

  "fromUnfolded" should "return monotonically increasing (until it overflows) stream of integers" in {
    val stream = Stream.fromUnfolded(10)
    stream.take(3).toList should equal (List(10, 11, 12))
  }

  "constantUnfolded" should "return infinite stream of constant values" in {
    val stream = Stream.constantUnfolded(5)
    stream.take(5).toList should equal (List(5,5,5,5,5))
    stream.take(7).toList should equal (List(5,5,5,5,5,5,5))
  }

  "onesUnfolded" should "return infinite stream of 1" in {
    Stream.onesUnfolded.take(4).toList should equal (List(1,1,1,1))
  }

  "mapViaUnfold" should "return a stream after element-by-element projection" in {
    val stream = Stream(2,4)
    stream.mapViaUnfold(x => (x * 2).toString).toList should equal (List("4", "8"))
  }

  "takeViaUnfold" should "take given number of elements from the stream" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.takeViaUnfold(2).toList should equal (Stream(1,2).toList)
  }

  "takeViaUnfold" should "return empty string if the number taken is <= 0" in {
    val stream = Stream(1,2,3,4,5,6)
    stream.takeViaUnfold(0) should equal (Stream.empty)
    stream.takeViaUnfold(-2) should equal (Stream.empty)
  }

  "takeWhileViaUnfold" should "take elements from the stream as long as predicate holds" in {
    val stream = Stream(2,4,6,77,2,11)
    stream.takeWhileViaUnfold(_ % 2 == 0).toList should equal (List(2,4,6))
  }

  "zipWith" should "zip two streams via projection as long as both streams have elements" in {
    val s1 = Stream(20, 10, 5)
    val s2 = Stream(10, 1)

    s1.zipWith(s2)(_*_).toList should equal (List(200, 10))
  }

  "zipAll" should "return elements as long as either of the streams has elements" in {
    val s1 = Stream("a","b","c")
    val s2 = Stream(1,2)

    s1.zipAll(s2).toList should equal (List((Some("a"), Some(1)), (Some("b"), Some(2)), (Some("c"), None)))
  }

  "startsWith" should "return true if stream starts with another stream" in {
    val s1 = Stream(1,2,3,4)
    val s2 = Stream(1,2)
    s1.startsWith(s2) should equal (true)
  }

  "startsWith" should "return false if stream does not start with another stream" in {
    val s1 = Stream(1,2,3,4)
    val s2 = Stream(3,4)
    s1.startsWith(s2) should equal (false)
  }

  "tails" should "return all tails of the stream" in {
    val s1 = Stream(1,2,3)
    s1.tails.mapViaUnfold(_.toList).toList should equal (List(List(1,2,3), List(2,3), List(3), Nil))
  }

  "scanRight" should "work like fold with immediate results" in {
    Stream(1,2,3).scanRight(0)(_+_).toList should equal (List(6,5,3,0))
  }
}
