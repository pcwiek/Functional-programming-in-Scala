package Chapter2
import org.scalatest._

class BasicsTests extends FlatSpec with Matchers {
  "Fib sequence" should "return correct number" in {
    val results = (1 to 6) map Basics.fib
    val expected = List(0, 1, 1, 2, 3, 5)
    results should contain theSameElementsInOrderAs expected
  }

  "isSorted" should "properly check for given ordering" in {
    val array = Array(1,2,3,4,5)
    val comparer = (a : Int, b : Int) => a > b
    Basics.isSorted(array, comparer) should equal (true)
    val unsorted = Array(1,2,3,2,4,5)
    Basics.isSorted(unsorted, comparer) should equal (false)
  }

  "isSorted2" should "work the same as recursive version" in {
    val array = Array(1,2,3,4,5)
    val comparer = (a : Int, b : Int) => a > b
    Basics.isSorted2(array, comparer) should equal (true)
    val unsorted = Array(1,2,3,2,4,5)
    Basics.isSorted2(unsorted, comparer) should equal (false)
  }

  "curry" should "properly curry a function" in {
    val fn = (a : Int, b : Int) => a + b
    val curried = Basics.curry(fn)
    curried(2)(3) should equal (5)
  }

  "compose" should "compose two functions (for a lack of a better description)" in {
    val fn1 = (a : String) => a.length
    val fn2 = (a : Int) => a % 2 == 0
    val composed = Basics.compose(fn2, fn1)
    composed("abc") should equal (false)
  }
}
