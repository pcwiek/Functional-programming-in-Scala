package Chapter5

sealed trait Stream[+A] {
  def foldRight[B](z: => B)(f: (A, B) => B): B =
    this match {
      case Cons(h, t) => f(h(), t().foldRight(z)(f))
      case _ => z
    }

  // Exercise 5.1
  def toList: List[A] = {
    @scala.annotation.tailrec
    def loop(str: Stream[A], acc: List[A]): List[A] = str match {
      case Empty => acc
      case Cons(x, xs) => loop(xs(), x() :: acc)
    }
    loop(this, Nil).reverse
  }

  def toListSimpleUnsafe: List[A] = {
    this match {
      case Empty => Nil
      case Cons(x, xs) => x() :: xs().toListSimpleUnsafe
    }
  }

  def toListImperative: List[A] = {
    val buffer = scala.collection.mutable.ListBuffer.empty[A]

    @scala.annotation.tailrec
    def loop(str: Stream[A]): List[A] = str match {
      case Empty => buffer.toList
      case Cons(x, xs) => buffer += x(); loop(xs())
    }
    loop(this)
  }

  // Exercise 5.2
  def take(n: Int): Stream[A] = {
    if (n > 0)
      this match {
        case Empty => Empty
        case Cons(x, xs) => Stream.cons(x(), xs().take(n - 1))
      }
    else Empty
  }

  def drop(n: Int): Stream[A] = {
    @scala.annotation.tailrec
    def loop(str: Stream[A], i: Int): Stream[A] = {
      if (i == 0) str
      else str match {
        case Empty => Empty
        case Cons(x, xs) => loop(xs(), i - 1)
      }
    }
    if (n > 0)
      loop(this, n)
    else this
  }

  // Exercise 5.3
  def takeWhile(p: A => Boolean): Stream[A] = {
    this match {
      case Cons(x, xs) if p(x()) => Stream.cons(x(), xs().takeWhile(p))
      case _ => Empty
    }
  }

  // Exercise 5.4
  def forAll(p: A => Boolean): Boolean = {
    this match {
      case Empty => true
      case Cons(x, xs) if p(x()) => xs().forAll(p)
      case _ => false
    }
  }

  // Exercise 5.5
  def takeWhileViaFoldRight(p: A => Boolean): Stream[A] = {
    this.foldRight(Stream.empty[A])((e, acc) => if (p(e)) Stream.cons(e, acc) else Stream.empty)
  }

  // Exercise 5.6
  def headOption: Option[A] = {
    this.foldRight(None: Option[A])((e, _) => Some(e))
  }

  // Exercise 5.7
  def mapViaFoldRight[B](f: A => B): Stream[B] = {
    foldRight(Stream.empty[B])((e, acc) => Stream.cons(f(e), acc))
  }

  def filterViaFoldRight(f: A => Boolean): Stream[A] = {
    foldRight(Stream.empty[A])((e, acc) => if (f(e)) Stream.cons(e, acc) else acc)
  }

  def appendViaFoldRight[B >: A](s: => Stream[B]): Stream[B] = {
    foldRight(s)((e, acc) => Stream.cons(e, acc))
  }

  // Exercise 5.13
  def mapViaUnfold[B](f: A => B) = {
    Stream.unfold(this) {
      case Empty => None
      case Cons(x, xs) => Some(f(x()), xs())
    }
  }

  def takeViaUnfold(n: Int) =
    Stream.unfold((this, n)) {
      case (Cons(x, xs), i) if i > 0 => Some(x(), (xs(), i - 1))
      case _ => None
    }

  def takeWhileViaUnfold(p: A => Boolean) = {
    Stream.unfold(this) {
      case Cons(x, xs) if p(x()) => Some(x(), xs())
      case _ => None
    }
  }

  def zipWith[B, C](stream: Stream[B])(f: (A, B) => C): Stream[C] = {
    Stream.unfold((this, stream)) {
      case (Cons(x1, xs1), Cons(x2, xs2)) => Some((f(x1(), x2()), (xs1(), xs2())))
      case _ => None
    }
  }

  def zipWithAll[B, C](stream: Stream[B])(f: (Option[A], Option[B]) => C) = {
    Stream.unfold((this, stream)) {
      case (Cons(x1, xs1), Cons(x2, xs2)) => Some(f(Some(x1()), Some(x2())), xs1() -> xs2())
      case (Cons(x, xs), _) => Some(f(Some(x()), Option.empty[B]), xs() -> Stream.empty[B])
      case (_, Cons(x, xs)) => Some(f(Option.empty[A], Some(x())), Stream.empty[A] -> xs())
      case _ => None
    }
  }

  def zipAll[B](stream: Stream[B]): Stream[(Option[A], Option[B])] = {
    zipWithAll(stream)(_ -> _)
  }

  // Exercise 5.14
  def startsWith[B](stream: Stream[B]): Boolean = {
    zipAll(stream).filterViaFoldRight(_._2.isDefined).forAll { case (a, b) => a == b}
  }

  // Exercise 5.15
  def tails =
    Stream.unfold(this) {
      case Empty => None
      case s => Some(s, s.drop(1))
    }.appendViaFoldRight(Stream(Empty))


  // Exercise 5.16
  def scanRight[B](z: B)(f: (A, B) => B): Stream[B] = {
    foldRight((z, Stream(z))) { case (accHead, (h, str)) =>
      val e = f(accHead, h)
      (e, Stream.cons(e, str))
    }
  }._2
}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }
  def empty[A]: Stream[A] = Empty
  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  // Exercise 5.8
  def constant[A](a : A) : Stream[A] = {
    lazy val str : Stream[A] = Stream.cons(a, str)
    str
  }

  // Exercise 5.9
  def from(n : Int) : Stream[Int] = {
    Stream.cons(n, from(n + 1))
  }

  // Exercise 5.10
  def fibs : Stream[Int] = {
    def loop(prev : Int, curr : Int) : Stream[Int] = {
      Stream.cons(prev, loop(curr, prev + curr))
    }
    loop(0, 1)
  }

  // Exercise 5.11
  def unfold[A, S](z : S)(f : S => Option[(A,S)]) : Stream[A] = {
    f(z) match {
      case Some((e,s)) => Stream.cons(e, unfold(s)(f))
      case None => Empty
    }
  }

  // Exercise 5.12
  def fibsUnfolded = {
    unfold((0,1)) { case (prev, curr) => Some((prev, (curr, prev + curr))) }
  }

  def fromUnfolded(n : Int) = {
    unfold(n)(i => Some(i, i+1))
  }

  def constantUnfolded[A](a : A) = {
    unfold(a)(e => Some((e, e)))
  }

  def onesUnfolded = {
    unfold(1)(_ => Some(1,1))
  }

}