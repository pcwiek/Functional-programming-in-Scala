package Chapter2

import scala.annotation.tailrec

object Basics {
  def fib(n : Int) = {
    @tailrec
    def loop(prev : Int, curr : Int, count : Int) : Int =
      if(n <= 1) { prev } else
      if(n == 2) { curr } else
      if(count < n) { loop(curr, curr + prev, count + 1) } else { curr }
    loop(0, 1, 2)
  }

  def isSorted[A](as : Array[A], ordered : (A,A) => Boolean) = {
    @tailrec
    def loop(index : Int, prev : A) : Boolean = {
      if(index == as.length) true
      else if(ordered(as(index), prev)) loop(index + 1, as(index))
      else false
    }
    loop(1, as(0))

  }

  def isSorted2[A](as : Array[A], ordered : (A, A) => Boolean) = {
    as.zip(as.drop(1)).find {case (b,a) => !ordered(a,b)}.isEmpty
  }

  def curry[A,B,C](f : (A,B) => C) : A => B => C = {
    g : A => h : B => f(g,h)
  }

  def compose[A,B,C](f : B => C, g :  A => B) : A => C = {
    h : A => f(g(h))
  }
}
