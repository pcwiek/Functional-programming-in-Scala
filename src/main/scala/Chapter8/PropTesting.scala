package Chapter8

import Chapter6.{RNG, State}
import Chapter8.Prop._

object Prop {
  type SuccessCount = Int
  type TestCases = Int
  type MaxSize = Int
  type FailedCase = String

  sealed trait Result {
    def isFalsified: Boolean
  }
  case object Passed extends Result {
    def isFalsified = false
  }
  case class Falsified(failure: FailedCase,
                       successes: SuccessCount) extends Result {
    def isFalsified = true
  }
  case object Proved extends Result {
    def isFalsified = false
  }
}

case class Prop(run: (MaxSize,TestCases,RNG) => Prop.Result) {
  def &&(p: Prop) = Prop {
    (max,n,rng) => run(max,n,rng) match {
      case Passed | Proved => p.run(max, n, rng)
      case x => x
    }
  }
  def ||(p: Prop) = Prop {
    (max,n,rng) => run(max,n,rng) match {
      // In case of failure, run the other prop.
      case Falsified(msg, _) => p.tag(msg).run(max,n,rng)
      case x => x
    }
  }
  /* This is rather simplistic - in the event of failure, we simply prepend
  * the given message on a newline in front of the existing message.
  */
  def tag(msg: String) = Prop {
    (max,n,rng) => run(max,n,rng) match {
      case Falsified(e, c) => Falsified(msg + "\n" + e, c)
      case x => x
    }
  }
}

case class Gen[+A](sample : State[RNG,A]) {
  // Exercise 8.6
  def flatMap[B](f : A => Gen[B]) = {
    Gen(sample.flatMap(s => f(s).sample))
  }

  // Exercise 8.10
  def unsized = SGen(_ => this)

  // Exercise 8.11 (part 1/2)
  def map[B](f : A => B) = {
    Gen(sample.map(f))
  }

  def map2[B,C](g: Gen[B])(f: (A,B) => C): Gen[C] =
    Gen(sample.map2(g.sample)(f))

  def **[B](g: Gen[B]): Gen[(A,B)] =
    map2(g)((_,_))
}

case class SGen[+A](forSize: Int => Gen[A]) {
  // Exercise 8.11 (part 2/2)
  def apply(n: Int): Gen[A] = forSize(n)

  def map[B](f: A => B): SGen[B] =
    SGen(i => forSize(i).map(f))

  def flatMap[B](f: A => Gen[B]): SGen[B] =
    SGen(i => forSize(i).flatMap(f))

  def **[B](s2: SGen[B]): SGen[(A, B)] =
    SGen(n => forSize(n) ** s2(n))
}

object Gen {
  // Exercise 8.4
  def choose(start : Int, stopExclusive : Int) : Gen[Int] = {
    Gen(State(RNG.nonNegativeInt).map(i => start + i % (stopExclusive - start)))
  }

  // Exercise 8.5
  def unit[A](a : => A) = {
    Gen(State.unit(a))
  }

  def boolean  = {
    Gen(State(RNG.int).map(_ % 2 == 0))
  }

  def listOfN[A](n : Int, g : Gen[A]) = {
    Gen(State.sequence(List.fill(n)(g.sample)))
  }

  // Exercise 8.7
  def union[A](g1 : Gen[A], g2 : Gen[A]) = {
    Gen.boolean.flatMap(b => if(b) g1 else g2)
  }

  // Exercise 8.8
  def weighted[A](g1 : (Gen[A], Double), g2: (Gen[A], Double)) = {
    val (gen1, p1) = g1
    val (gen2, p2) = g2
    val thr = p1.abs / (p1.abs + p2.abs)
    Gen(State(RNG.double).flatMap(d => if (d < thr) gen1.sample else gen2.sample))
  }

  // Exercise 8.12
  def listOf[A](g : Gen[A]) : SGen[List[A]] = {
    SGen(Gen.listOfN(_, g))
  }

  // Exercise 8.13
  def listOf1[A](g : Gen[A]) = {
    SGen(_ => Gen.listOfN(1, g))
  }
}
