package Chapter7

import java.util.concurrent.{Callable, Future, TimeUnit, ExecutorService}

object Par {
  type Par[A] = ExecutorService => Future[A]
  def run[A](s: ExecutorService)(a: Par[A]): Future[A] = a(s)

  def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

  private case class UnitFuture[A](get: A) extends Future[A] {
    def isDone = true
    def get(timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel(evenIfRunning: Boolean): Boolean = false
  }

  def map2[A,B,C](a: Par[A], b: Par[B])(f: (A,B) => C): Par[C] =
    (es: ExecutorService) => {
      val af = a(es)
      val bf = b(es)
      UnitFuture(f(af.get, bf.get))
    }

  def fork[A](a: => Par[A]): Par[A] =
    es => es.submit(new Callable[A] {
      def call = a(es).get
    })

  def map[A,B](pa: Par[A])(f: A => B): Par[B] =
    map2(pa, unit(()))((a,_) => f(a))

  def sortPar(parList: Par[List[Int]]) = map(parList)(_.sorted)

  // Exercise 7.4
  def asyncF[A,B](f: A => B): (A) => Par[B] = a => fork(unit(f(a)))

  // Exercise 7.5
  def sequence[A](ps : List[Par[A]]) : Par[List[A]] = {
    ps.foldRight(Par.unit[List[A]](Nil))(map2(_,_)(_::_))
  }

  // Exercise 7.6
  def parFilter[A](as : List[A])(f : A => Boolean) : Par[List[A]] = {
    val flt: List[Par[List[A]]] = as.map(asyncF(a => if (f(a)) List(a) else Nil))
    map(sequence(flt))(_.flatten)
  }

  // Exercise 7.11
  def choiceN[A](n : Par[Int])(choices : List[Par[A]]) : Par[A] = {
    es =>
      val index = run(es)(n).get
      run(es)(choices(index))
  }

  // Exercise 7.12
  def choiceMap[K,V](key : Par[K])(choices : Map[K, Par[V]]) : Par[V] = {
    es => {
      val k = run(es)(key).get()
      run(es)(choices(k))
    }
  }

  // Exercise 7.13
  def flatMap[A,B](a : Par[A])(f: A => Par[B]) : Par[B] = {
    es => {
      val arg = run(es)(a).get()
      run(es)(f(arg))
    }
  }

  // Exercise 7.14
  def join[A](a : Par[Par[A]]) : Par[A] = {
    flatMap(a)(identity)
  }
}