package Chapter6

case class State[S, +A](transition : S => (A,S)) {
  // Exercise 6.10 (part)
  def flatMap[B](f : A => State[S,B]) : State[S,B] = {
    State(s => {
      val (a,s1) = transition(s)
      f(a).transition(s1)
    })
  }

  def map_1[B](f : A => B) : State[S,B] = {
    State(s => {
      val (a, s1) = transition(s)
      (f(a), s1)
    })
  }

  def map[B](f : A => B) : State[S,B] = {
    flatMap(f andThen State.unit)
  }

  def map2[B,C](s2 : State[S,B])(f : (A,B) => C) : State[S,C] = {
    flatMap(a => s2.map(b => f(a,b)))
  }


}

object State {
  def get[S1]: State[S1, S1] = State(s => (s, s))
  def set[S1](s: S1): State[S1, Unit] = State(_ => ((), s))
  def modify[S1](f: S1 => S1): State[S1, Unit] = for {
    s <- get
    _ <- set(f(s))
  } yield ()

  // Exercise 6.10 (part)
  def unit[S, A](a : A): State[S, A] = State(s => (a,s))

  def sequence[S,A](lst : List[State[S,A]]) : State[S, List[A]] = {
    lst.foldRight(State.unit[S, List[A]](Nil))((el, s) => el.map2(s)(_ :: _))
  }
}

// Exercise 6.11
sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int)

object FSM {
  def simulate(input: List[Input]): State[Machine, (Int, Int)] = {
    val transitions = input.map(i => State.modify[Machine](s => (i, s) match {
      case (Coin, Machine(true, cd, co)) if cd > 0 => s.copy(locked = false, coins = co + 1)
      case (Turn, Machine(false, cd, co)) => s.copy(locked = true, candies = cd - 1)
      case _ => s
    }))
    for {
      _ <- State.sequence(transitions)
      st <- State.get
    } yield (st.candies, st.coins)
  }

  def simulateDesugared(input: List[Input]): State[Machine, (Int, Int)] = {
    val transitions = input.map(i => State.modify[Machine](s => (i, s) match {
      case (Coin, Machine(true, cd, co)) if cd > 0 => s.copy(locked = false, coins = co + 1)
      case (Turn, Machine(false, cd, co)) => s.copy(locked = true, candies = cd - 1)
      case _ => s
    }))
    State.sequence(transitions)
      .flatMap(_ => State.get[Machine])
      .map(m => (m.candies, m.coins))
  }
}