package Chapter6


trait RNG {
  def nextInt: (Int, RNG)
}

case class SimpleRNG(seed: Long) extends RNG {
  def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
    val nextRNG = SimpleRNG(newSeed)
    val n = (newSeed >>> 16).toInt
    (n, nextRNG)
  }
}

object RNG {
  type Rand[+A] = RNG => (A, RNG)

  def int(rng: RNG) = rng.nextInt

  // Exercise 6.1
  def nonNegativeInt(rng: RNG) = {
    val (n, r) = rng.nextInt
    val value = if (n < 0) -(n + 1) else n
    (value, r)
  }

  // Exercise 6.2
  def double(rng: RNG) = {
    val (n, r) = nonNegativeInt(rng)
    (n.toDouble / Int.MaxValue, r)
  }

  // Exercise 6.3
  def intDouble(rng: RNG) = {
    val (i, r1) = rng.nextInt
    val (d, r2) = double(r1)
    (i -> d, r2)
  }

  def doubleInt(rng: RNG) = {
    val (d, r1) = double(rng)
    val (i, r2) = r1.nextInt
    (d -> i, r2)
  }

  def double3(rng: RNG) = {
    val (d1, r1) = double(rng)
    val (d2, r2) = double(r1)
    val (d3, r3) = double(r2)
    ((d1, d2, d3), r3)
  }

  // Exercise 6.4
  def ints(count: Int)(rng: RNG) = {
    @scala.annotation.tailrec
    def loop(r: RNG, acc: List[Int], i: Int): (List[Int], RNG) = {
      if (i < count) {
        val (value, r1) = r.nextInt
        loop(r1, value :: acc, i + 1)
      }
      else {
        acc -> r
      }
    }
    loop(rng, Nil, 0)
  }

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a, rng2) = s(rng)
      (f(a), rng2)
    }

  // Exercise 6.5
  def doubleViaMap = {
    map(nonNegativeInt)(_.toDouble / Int.MaxValue)
  }

  // Exercise 6.6
  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    rng => {
      val (a, rng2) = ra(rng)
      val (b, rng3) = rb(rng2)
      (f(a, b), rng3)
    }

  // Exercise 6.7
  def sequence[A](fs : List[Rand[A]]) : Rand[List[A]] = {
    val seed : Rand[List[A]] = rng => (Nil, rng)
    fs.foldRight(seed)((el, acc) => map2(el, acc)(_ :: _))
  }

  // Exercise 6.8
  def flatMap[A,B](f : Rand[A])(g : A => Rand[B]) : Rand[B] = {
    rng => {
      val (v, r) = f(rng)
      g(v)(r)
    }
  }

  // Exercise 6.9
  def mapViaFlatMap[A,B](s: Rand[A])(f: A => B): Rand[B] = {
    flatMap(s)(a => r => (f(a), r))
  }

  def map2ViaFlatMap[A,B,C](ra : Rand[A], rb : Rand[B])(f : (A, B) => C) : Rand[C] = {
    flatMap(ra)(a => mapViaFlatMap(rb)(b => f(a,b)))
  }
}
