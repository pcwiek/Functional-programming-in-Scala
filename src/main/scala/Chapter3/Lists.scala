package Chapter3

import scala.annotation.tailrec

// Implementation given in the book
// --------------------------------
sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]
object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x,xs) => x + sum(xs)
  }
  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x,xs) => x * product(xs)
  }
  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B =
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }
// --------------------------------

  // Exercise 3.2
  def tailEx[A](l : List[A]) = {
    l match {
      case Nil => sys.error("Empty list!")
      case Cons(_, xs) => xs
    }
  }
  def tailEither[A](l : List[A]) = {
    l match {
      case Nil => Left("Empty list has no tail")
      case Cons(_, xs) => Right(xs)
    }
  }

  def tailOption[A](l : List[A]) = {
    l match {
      case Nil => None
      case Cons(_, xs) => Some(xs)
    }
  }

  // Exercise 3.3
  def setHead[A](head : A, l : List[A]) = {
    l match {
      case Nil => Cons(head, Nil)
      case Cons(_, xs) => Cons(head, xs)
    }
  }

  // Exercise 3.4
  def drop[A](n : Int, l : List[A]) = {
    @tailrec
    def loop(i : Int, lst : List[A]) : List[A] = {
      if(i >= n) lst
      else lst match {
        case Nil => Nil
        case Cons(_, xs) => loop(i+1, xs)
      }
    }
    loop(0, l)
  }

  // Exercise 3.5
  def dropWhile[A](l : List[A], f : A => Boolean) = {
    @tailrec
    def loop(lst : List[A]) : List[A] = {
      lst match {
        case Nil => Nil
        case Cons(x, xs) if f(x) => loop(xs)
        case _ => lst
      }
    }
    loop(l)
  }

  // Exercise 3.6
  def init[A : scala.reflect.ClassTag](l : List[A]) = {
    import scala.collection.mutable.ListBuffer
    val buffer = ListBuffer.empty[A]
    @tailrec
    def loop(lst : List[A]) : List[A] = {
      lst match {
        case Nil | Cons(_, Nil) => List(buffer.toArray : _*)
        case Cons(x, xs) => buffer += x; loop(xs)
      }
    }
    loop(l)
  }

  // Exercise 3.9
  def length[A](as : List[A]) = {
    List.foldRight(as, 0)((_, acc) => acc + 1)
  }

  // Exercise 3.10
  @tailrec
  def foldLeft[A, B](as : List[A], z : B)(op : (B, A) => B) : B = {
    as match {
      case Nil => z
      case Cons(x, xs) => foldLeft(xs, op(z, x))(op)
    }
  }

  // Exercise 3.11
  def sumViaFoldLeft(as : List[Int]) = {
    List.foldLeft(as, 0)(_ + _)
  }

  def productViaFoldLeft(as : List[Int]) = {
    List.foldLeft(as, 1)(_ * _)
  }

  def lengthViaFoldLeft[A](as : List[A]) = {
    List.foldLeft(as, 0)((acc, _) => acc + 1)
  }

  // Exercise 3.12
  def reverse[A](l : List[A]) = {
    List.foldLeft(l, Nil : List[A])((acc, e) => Cons(e, acc))
  }

  // Exercise 3.14
  def append[A](l : List[A], r : List[A]) = {
    foldRight(l, r)(Cons(_,_))
  }

  // Exercise 3.15
  def concat[A](l : List[List[A]]) = {
    foldLeft(l, Nil : List[A])(append)
  }

  // Exercise 3.16
  def addOne(l : List[Int]) = {
    foldRight(l, Nil : List[Int])((e, acc) => Cons(e+1, acc))
  }

  // Exercise 3.17
  def numbersToStrings(l : List[Double]) = {
    foldRight(l, Nil : List[String])((e, acc) => Cons(e.toString, acc))
  }

  // Exercise 3.18
  def map[A,B : scala.reflect.ClassTag](l : List[A])(f : A => B) = {
    import scala.collection.mutable.ListBuffer
    val buffer = ListBuffer.empty[B]
    @tailrec
    def loop(lst : List[A]) : List[B] = {
      lst match {
        case Nil => List(buffer.toArray : _*)
        case Cons(x, xs) => buffer += f(x); loop(xs)
      }
    }
    loop(l)
  }

  def mapFoldRight[A,B](l : List[A])(f: A => B) = {
    foldRight(l, Nil : List[B])((e, acc) => Cons(f(e), acc))
  }

  // Exercise 3.19
  def filter[A : scala.reflect.ClassTag](l : List[A])(f : A => Boolean) = {
    import scala.collection.mutable.ListBuffer
    val buffer = ListBuffer.empty[A]
    @tailrec
    def loop(lst : List[A]) : List[A] = {
      lst match {
        case Nil => List(buffer.toArray : _*)
        case Cons(x, xs) if f(x) => buffer += x; loop(xs)
        case Cons(_, xs) => loop(xs)
      }
    }
    loop(l)
  }

  // Exercise 3.20
  def flatMapViaFold[A,B](l : List[A])(f : A => List[B]) : List[B] = {
    foldRight(l, Nil : List[B])((e, acc) => append(f(e), acc))
  }

  def flatMap[A,B](l : List[A])(f : A => List[B]) : List[B] = {
    concat(map(l)(f))
  }

  // Exercise 3.21
  def filterViaFlatMap[A](l : List[A])(f : A => Boolean) = {
    flatMap(l)(e => if (f(e)) Cons(e,Nil) else Nil)
  }

  // Exercise 3.22
  def addPairs(l : List[Int], r : List[Int]) : List[Int] = {
    (l, r) match{
      case (Nil, _) | (_, Nil) => Nil
      case (Cons(x1, xs1), Cons(x2, xs2)) => Cons(x1 + x2, addPairs(xs1, xs2))
    }
  }

  // Exercise 3.23
  def zipWith[A, B, C](l : List[A], r : List[B])(f : (A,B) => C) = {
    @tailrec
    def loop(acc : List[C], left : List[A], right : List[B]) : List[C] = {
      (left, right) match {
        case (Nil, _) | (_ , Nil) => reverse(acc)
        case (Cons(x1, xs1), Cons(x2, xs2)) => loop(Cons(f(x1,x2), acc), xs1, xs2)
      }
    }
    loop(Nil, l, r)
  }

  // Exercise 3.24
  def hasSubsequence[A](l : List[A], subseq : List[A]) = {
    @tailrec
    def loop(lst : List[A], subs : List[A]) : Boolean = {
      (lst, subs) match {
        case (Nil, _) => false
        case (_, Nil) => true
        case (Cons(x1, xs1), Cons(x2, xs2)) if x1 == x2 => loop(xs1, xs2)
        case (Cons(_, xs1), _) => loop(xs1, subseq)
      }
    }
    loop(l, subseq)
  }
}