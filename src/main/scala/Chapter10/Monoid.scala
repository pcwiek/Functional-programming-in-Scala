package Chapter10

trait Monoid[A] {
  def op(a1 : A, a2 : A) : A
  def zero : A
}

object Monoid {
  val stringMonoid = new Monoid[String] {
    def op(a1: String, a2: String) = a1 + a2

    val zero = ""
  }

  def listMonoid[A] = new Monoid[List[A]] {
    def op(a1: List[A], a2: List[A]) = a1 ++ a2

    val zero = Nil
  }

  // Exercise 10.1
  val intAddition = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 + a2

    override def zero: Int = 0
  }
  val intMultiplication = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 * a2

    override def zero: Int = 1
  }
  val booleanOr = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 || a2

    override def zero: Boolean = false
  }
  val booleanAnd = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 && a2

    override def zero: Boolean = true
  }

  // Exercise 10.10
  val wcMonoid = new Monoid[WordCount] {
    override def op(a1: WordCount, a2: WordCount): WordCount = (a1, a2) match {
      case (Part(l1, w1, r1), Part(l2, w2, r2)) => Part(l1, w1 + (if ((r1 + l2).isEmpty) 0 else 1) + w2, r2)
      case (Stub(s1), Stub(s2)) => Stub(s1 + s2)
      case (Part(l, w, r), Stub(s)) => Part(l, w, r + s)
      case (Stub(s), Part(l, w, r)) => Part(s + l, w, r)
    }

    override def zero: WordCount = Stub("")
  }

  // Exercise 10.2
  def optionMonoid[A] = new Monoid[Option[A]] {
    override def op(a1: Option[A], a2: Option[A]): Option[A] = a1.orElse(a2)

    override def zero: Option[A] = None
  }

  // Exercise 10.3
  def endoMonoid[A] = new Monoid[A => A] {
    override def op(a1: (A) => A, a2: (A) => A): (A) => A = a1.compose(a2)

    override def zero: (A) => A = identity
  }

  def concatenate[A](as: List[A], m: Monoid[A]): A =
    as.foldLeft(m.zero)(m.op)

  // Exercise 10.5
  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.foldLeft(m.zero)((acc, v) => m.op(acc, f(v)))

  // Exercise 10.7
  def foldMapV[A, B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): B = {
    v.length match {
      case 0 => m.zero
      case 1 => f(v(0))
      case _ =>
        val (l, r) = v.splitAt(v.length / 2)
        m.op(foldMapV(l, m)(f), foldMapV(r, m)(f))
    }
  }

  // Exercise 10.16
  def productMonoid[A, B](a: Monoid[A], b: Monoid[B]): Monoid[(A, B)] = new Monoid[(A, B)] {
    override def op(A: (A, B), B: (A, B)) : (A, B) = (A, B) match {
      case ((a1, b1), (a2, b2)) => (a.op(a1, a2), b.op(b1,b2))
    }
    override def zero: (A, B) = (a.zero, b.zero)
  }


  def mapMergeMonoid[K,V](V: Monoid[V]): Monoid[Map[K, V]] =
    new Monoid[Map[K, V]] {
      def zero = Map[K,V]()
      def op(a: Map[K, V], b: Map[K, V]) =
        (a.keySet ++ b.keySet).foldLeft(zero) { (acc,k) =>
          acc.updated(k, V.op(a.getOrElse(k, V.zero),
            b.getOrElse(k, V.zero)))
        }
    }

  // Exercise 10.17
  def functionMonoid[A,B](B: Monoid[B]) : Monoid[A => B] =
    new Monoid[A => B] {
      override def op(a1: (A) => B, a2: (A) => B): (A) => B = arg => B.op(a1(arg), a2(arg))

      override def zero: (A) => B = a => B.zero
    }

  // Exercise 10.18
  def bag[A](as : IndexedSeq[A]) : Map[A, Int] = {
    val m = mapMergeMonoid[A, Int](Monoid.intAddition)
    foldMapV(as, m)(e => Map(e -> 1))
    // Alternatively: as.map(e => Map(e -> 1)).foldLeft(m.zero)(m.op)
  }
}

sealed trait WordCount
case class Stub(chars: String) extends WordCount
case class Part(lStub: String, words: Int, rStub: String) extends WordCount
