package Shared

object HelperImplicits {

  implicit class ForwardPipe[A](val src: A) extends AnyVal {
    def |>[B](f: A => B) = f(src)
  }

}
