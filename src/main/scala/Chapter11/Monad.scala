package Chapter11

trait Monad[F[_]] extends Functor[F]{
  def unit[A](a: => A) : F[A]
  def flatMap[A,B](ma : F[A])(f : A => F[B]) : F[B]

  def map[A,B](ma : F[A])(f : A => B) : F[B] =
    flatMap(ma)(a => unit(f(a)))

  def map2[A,B,C](ma : F[A], mb : F[B])(f : (A,B) => C) : F[C] =
    flatMap(ma)(a => map(mb)(b => f(a,b)))

  // Exercise 11.3
  def sequence[A](lma : List[F[A]]) : F[List[A]] = traverse(lma)(identity)
  def traverse[A,B](la : List[A])(f : A => F[B]) : F[List[B]] =
    la.foldRight(unit(Nil) : F[List[B]])((e, acc) => map2(f(e), acc)(_ :: _))

  // Exercise 11.4
  def replicateM[A](n : Int, ma : F[A]) : F[List[A]] =
    map(ma)(List.fill(n)(_))

  // Exercise 11.6
  def filterM[A](ma : List[A])(f : A => F[Boolean]) : F[List[A]] =
    ma match {
      case Nil => unit(Nil)
      case x :: xs => flatMap(f(x))(a => if(!a) filterM(xs)(f) else map(filterM(xs)(f))(x :: _))
    }

  // Exercise 11.7
  def compose[A,B,C](f: A => F[B], g : B => F[C]) : A => F[C] =
    a => flatMap(f(a))(g)

  // Exercise 11.8
  def flatMapComposed[A,B](ma : F[A])(f : A => F[B]) : F[B] =
   compose( (_ : Unit) => ma, f).apply()
}