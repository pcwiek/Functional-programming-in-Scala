package Chapter4

// Implementation given in the book
// --------------------------------
trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B]
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B]
  def orElse[EE >: E,B >: A](b: => Either[EE, B]): Either[EE, B]
  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C]
}
// --------------------------------

// Exercise 4.6
case class Left[+E](value: E) extends Either[E, Nothing] {
  override def map[B](f: (Nothing) => B): Either[E, B] = this

  override def map2[EE >: E, B, C](b: Either[EE, B])(f: (Nothing, B) => C): Either[EE, C] = this

  override def flatMap[EE >: E, B](f: (Nothing) => Either[EE, B]): Either[EE, B] = this

  override def orElse[EE >: E, B >: Nothing](b: => Either[EE, B]): Either[EE, B] = b
}

case class Right[+A](value: A) extends Either[Nothing, A] {
  override def map[B](f: (A) => B): Either[Nothing, B] = Right(f(value))

  override def map2[EE >: Nothing, B, C](b: Either[EE, B])(f: (A, B) => C) = {
    b.map(v => f(value, v))
  }

  override def flatMap[EE >: Nothing, B](f: (A) => Either[EE, B]) = f(value)

  override def orElse[EE >: Nothing, B >: A](b: => Either[EE, B]) = this
}

object Either {
  // Exercise 4.7
  def sequence[E,A](l : List[Either[E,A]]) = {
    traverse(l)(identity)
  }

  def traverse[E,A,B](l : List[A])(f : A => Either[E,B]) = {
    l.foldRight(Right(Nil) : Either[E, List[B]])((e, acc) => f(e).map2(acc)(_::_))
  }
}