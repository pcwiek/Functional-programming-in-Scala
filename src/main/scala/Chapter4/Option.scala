package Chapter4

// Implementation given in the book
// --------------------------------
sealed trait Option[+A] {
  def map[B](f: A => B): Option[B]
  def flatMap[B](f: A => Option[B]): Option[B]
  def getOrElse[B >: A](default: => B): B
  def orElse[B >: A](ob: => Option[B]): Option[B]
  def filter(f: A => Boolean): Option[A]
}
// --------------------------------

// Exercise 4.1
case class Some[+A](value: A) extends Option[A] {
  override def map[B](f: (A) => B) = Some(f(value))

  override def flatMap[B](f: (A) => Option[B]) = f(value)

  override def filter(f: (A) => Boolean) = if (f(value)) this else None

  override def getOrElse[B >: A](default: => B) = value

  override def orElse[B >: A](ob: => Option[B]) = this
}

case object None extends Option[Nothing] {
  override def map[B](f: (Nothing) => B) = None

  override def flatMap[B](f: (Nothing) => Option[B]) = None

  override def filter(f: (Nothing) => Boolean) = None

  override def getOrElse[B >: Nothing](default: => B) = default

  override def orElse[B >: Nothing](ob: => Option[B]) = ob
}

object Option {
  // Exercise 4.2
  def mean(xs : Seq[Double]) = {
    if(xs.isEmpty) None
    else Some(xs.sum / xs.length)
  }

  def variance(xs : Seq[Double]) = {
    mean(xs).flatMap(m => mean(xs.map(x => math.pow(x - m, 2))))
  }

  // Exercise 4.3
  def map2[A,B,C](a : Option[A], b : Option[B])(f : (A,B) => C) = {
    (a, b) match {
      case (Some(v1), Some(v2)) => Some(f(v1,v2))
      case _ => None
    }
  }

  // Exercise 4.4
  def sequence[A](l : List[Option[A]]) = {
    @scala.annotation.tailrec
    def loop(lst : List[Option[A]], acc : List[A]) : Option[List[A]] = {
      lst match {
        case Nil => Some(acc.reverse)
        case Some(x) :: xs => loop(xs, x :: acc)
        case _ => None
      }
    }
    loop(l, Nil)
  }

  // Exercise 4.5
  def traverseNaive[A,B](a : List[A])(f : A => Option[B]) : Option[List[B]] = {
    sequence(a.map(f))
  }

  def traverse[A,B](a : List[A])(f : A => Option[B]) : Option[List[B]] = {
    a match {
      case Nil => Some(Nil)
      case x :: xs => map2(f(x), traverse(xs)(f))((el, acc) => el :: acc)
    }
  }

  def traverse2[A,B](l : List[A])(f : A => Option[B]) : Option[List[B]] = {
    l.foldRight(Some(Nil) : Option[List[B]])((e, acc) => map2(f(e), acc)(_::_))
  }
}