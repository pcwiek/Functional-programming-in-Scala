
name := "Functional programming in Scala"

version := "1.0.0"

scalaVersion := "2.11.1"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")
